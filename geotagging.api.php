<?php

/**
 * @file
 * These are the hooks that are invoked by the geotagging core.
 */

/**
 * Register new geotagging endpoint.
 *
 * @return array
 *   Params array.
 */
function hook_geotagging_endpoint() {
  return [
    'geotagging_mock' => [
      // Required param.
      'title' => t('Local Test Endpoint'),
      // Required param.
      'description' => t('This endpoint is used to test geotagging functionality on a single environment and does not share any geotagging data with other sites.'),
      // Required param.
      'url' => 'http://localhost',
      // // Required param. X-API-KEY http header required by endpoint.
      'x-api-key' => 'some key',
      // Optional param.
      'include' => 'path to file with form',
      // Optional param.
      'edit_form' => 'geotagging_mock_form',
    ],
  ];
}

/**
 * Allows modules to alter GeotaggingEndpointClient.
 *
 * @param GeotaggingEndpointClientInterface $client
 *   Object which implements GeotaggingEndpointClientInterface.
 * @param string $active_endpoint_key
 *   Machine name of active andpoint.
 */
function hook_geotagging_endpoint_client_alter(GeotaggingEndpointClientInterface $client, $active_endpoint_key) {
}
