<?php

/**
 * @file
 * Provides metatag functions for Custom Metatags.
 */

/**
 * Implements hook_metatag_info().
 */
function geotagging_metatag_info() {
  $info['groups']['geotagging'] = [
    'label' => t('Geotagging'),
    'form' => ['#collapsed' => FALSE],
  ];

  $info['tags']['content_family_id'] = [
    'label' => t('Content Family ID'),
    'description' => t("Family ID is used for geotagging content by language and region. The same Family ID should be added to each language and regional version of a page to help search engines provide the most relevant results based on the user's location."),
    'class' => 'GeotaggingAlternativeUrlMetaTag',
    'group' => 'geotagging',
  ];

  return $info;
}
