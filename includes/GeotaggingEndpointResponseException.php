<?php

/**
 * @file
 * Geotagging exception.
 */

/**
 * Class GeotaggingEndpointResponseException.
 */
class GeotaggingEndpointResponseException extends Exception {

  public $message = 'Geotagging endpoint response status code differs from expected';
  protected $httpCode;
  protected $httpHeaders;
  protected $responseData;

  /**
   * Get HTTP status code.
   *
   * @return int
   *   HTTP status code.
   */
  public function getHttpCode() {
    return $this->httpCode;
  }

  /**
   * Set HTTP status code.
   *
   * @param string $httpCode
   *   HTTP status code.
   */
  public function setHttpCode($httpCode) {
    $this->httpCode = $httpCode;
  }

  /**
   * Get HTTP response headers.
   *
   * @return array
   *   HTTP response headers
   */
  public function getHttpHeaders() {
    return $this->httpHeaders;
  }

  /**
   * Set HTTP response headers.
   *
   * @param array $httpHeaders
   *   Array of HTTP headers.
   */
  public function setHttpHeaders($httpHeaders) {
    $this->httpHeaders = $httpHeaders;
  }

  /**
   * Get response data.
   *
   * @return string
   *   Response data.
   */
  public function getResponseData() {
    return $this->responseData;
  }

  /**
   * Set response data.
   *
   * @param mixed $responseData
   *   Response data.
   */
  public function setResponseData($responseData) {
    $this->responseData = $responseData;
  }

  /**
   * Handler for other Response codes.
   *
   * @param object $response
   *   Response from Service.
   *
   * @throws \Exception
   *
   * @return GeotaggingEndpointResponseException|\Exception
   *   An exception.
   */
  public function handleErrorData($response) {
    if (property_exists($response, 'code') && is_numeric($response->code)) {

      $this->setHttpCode($response->code);

      if (property_exists($response, 'data')) {
        $this->setResponseData($response->data);
      }

      if (property_exists($response, 'headers')) {
        $this->setHttpHeaders($response->headers);
      }

      return $this;
    }
    else {
      throw new Exception(t('There was a problem retrieving the response data.') . $response->data);
    }
  }

}
