<?php

/**
 * @file
 * Interface IGeotaggingEndpointClient.
 */

/**
 * Geotagging endpoint description.
 */
interface GeotaggingEndpointClientInterface {

  /**
   * Put data to endpoint.
   *
   * @param string $contentFamilyId
   *   Alternative content family id.
   * @param string $hreflang
   *   Locale and language. Ex.  en_US.
   * @param string $alternativeUrl
   *   Url.
   *
   * @return int
   *   HTTP status code.
   *
   * @throws Exception
   */
  public function put($contentFamilyId, $hreflang, $alternativeUrl);

  /**
   * Pull urls from endpoint.
   *
   * @param string $contentFamilyId
   *   Alternative content family id.
   *
   * @return array
   *   Json decoded array.
   *
   * @throws Exception
   */
  public function pull($contentFamilyId);

  /**
   * Delete data from endpoint.
   *
   * @param string $contentFamilyId
   *   Alternative content family id.
   * @param string $hreflang
   *   Locale and language. Ex.  en_US.
   *
   * @return bool
   *   operation result
   *
   * @throws Exception
   */
  public function delete($contentFamilyId, $hreflang);

}
