<?php

/**
 * @file
 * Class GeotaggingAlternativeUrlMetaTag.
 */

/**
 * Class GeotaggingAlternativeUrlMetaTag.
 */
class GeotaggingAlternativeUrlMetaTag extends DrupalTextMetaTag {

  /**
   * {@inheritdoc}
   */
  public function getElement(array $options = []) {
    try {
      $elements = [];
      if (!empty($this->data['value'])) {
        $content_family_id = $this->getValue($options);
        if (!empty($content_family_id)) {

          $geotagging_endpoint = geotagging_get_endpoint_client();
          $geotagging_data = $geotagging_endpoint->pull($content_family_id);
          if (!empty($geotagging_data) && !empty($geotagging_data['alternates']) && is_array($geotagging_data['alternates'])) {

            foreach ($geotagging_data['alternates'] as $i => $params) {
              $elements[] = [
                [
                  '#type' => 'html_tag',
                  '#tag' => 'link',
                  // We added weight to group alternative tags together.
                  '#weight' => -100 + $i,
                  '#attributes' => [
                    'rel' => 'alternate',
                    'hreflang' => $params['hreflang'],
                    'href' => $params['href'],
                  ],
                ],
                'geotagging_link_' . $i,
              ];
            }
          }
        }
      }
      if (!empty($elements)) {
        return [
          '#attached' => [
            'drupal_add_html_head' => $elements,
          ],
        ];
      }
    }
    catch (GeotaggingEndpointResponseException $e) {
      _geotagging_response_exception_message($e);
    }
    catch (Exception $e) {
      _geotagging_exception_message($e);
    }
  }
}
