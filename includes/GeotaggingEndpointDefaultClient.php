<?php

/**
 * @file
 * Class GeotaggingEndpointDefaultClient.
 */

/**
 * Default endpoint client.
 */
class GeotaggingEndpointDefaultClient implements GeotaggingEndpointClientInterface {

  /**
   * Endpoint URL variable.
   *
   * @var string
   */
  protected $endpointUrl;

  /**
   * Endpoint X-API-KEY.
   *
   * @var string
   */
  protected $xApiKey;

  /**
   * HTTP Headers variable.
   *
   * @var array
   */
  protected $headers = [];

  /**
   * GeotaggingEndpointDefaultClient constructor.
   *
   * @param string $endpointUrl
   *   Endpoint URL.
   * @param array $headers
   *   HTTP Headers.
   */
  public function __construct($endpointUrl, $xApiKey, $headers = []) {
    $this->headers = $headers;
    $this->endpointUrl = $endpointUrl;
    $this->xApiKey = $xApiKey;
  }

  /**
   * Adds HTTP Header.
   *
   * @param string $key
   *   Name for the Header.
   * @param mixed $value
   *   Value for the Header.
   */
  public function addHeader($key, $value) {
    $this->headers[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function pull($family_id) {

    $url = $this->endpointUrl . '/' . $family_id;

    $response = $this->send('GET', $url, NULL, $this->headers);

    if ($response->code == '200') {
      return json_decode($response->data, TRUE);
    }
    else {
      $response_exception = new GeotaggingEndpointResponseException();
      $response_exception->handleErrorData($response);
      throw $response_exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function put($family_id, $href_lang, $alternative_url) {

    $url = $this->endpointUrl . '/' . $family_id . '/' . $href_lang;

    $this->headers['Content-Type'] = 'application/json';
    $data = json_encode(['href' => $alternative_url], JSON_FORCE_OBJECT);
    $response = $this->send('PUT', $url, $data, $this->headers);

    // Any 20x status code. Like 200, 201 or 409.
    if (in_array($response->code, [200, 201, 204, 409])) {
      return $response->code;
    }
    else {
      $response_exception = new GeotaggingEndpointResponseException();
      $response_exception->handleErrorData($response);
      throw $response_exception;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete($family_id, $href_lang) {

    $url = $this->endpointUrl . '/' . $family_id . '/' . $href_lang;

    $response = $this->send('DELETE', $url, NULL, $this->headers);

    if (property_exists($response, 'code') && in_array($response->code, [200, 204])) {
      return TRUE;
    }
    else {
      $response_exception = new GeotaggingEndpointResponseException();
      $response_exception->message = t('Expected status code for successful DELETE operation is 204, but endpoint responded with @code',
        ['@code' => $response->code]);
      $response_exception->handleErrorData($response);
      throw $response_exception;
    }
  }

  /**
   * Send a request.
   *
   * @param string $method
   *   HTTP method to use.
   * @param string $url
   *   URL to send the request.
   * @param mixed $body
   *   Request body.
   * @param array $headers
   *   Additional headers to send.
   * @param array $options
   *   Additional options to send.
   *
   * @return object
   *   Response.
   *
   * @throws \Exception
   *   If a drupal_http_request error occurs.
   */
  private function send($method, $url, $body = NULL, $headers = [], $options = []) {

    $geotagging_debug_option = variable_get(GEOTAGGING_DEBUG, NULL);

    $data = ['method' => $method];

    $headers['X-API-KEY'] = $this->xApiKey;

    if (!empty($headers)) {
      $data['headers'] = $headers;
    }

    if (!empty($body)) {
      $data['data'] = $body;
    }

    // A float representing the maximum number of seconds the function call may take.
    // If a timeout occurs, the error code is set to the HTTP_REQUEST_TIMEOUT constant.
    if($method == 'GET') {
      // We set timeout for GET only because sometimes DELETE and PUT need more then 1s.
      $data += array(
        'timeout' => 1.0,
      );
    }

    $response = drupal_http_request($url, $data);

    $not_success_code = (
      $geotagging_debug_option == 1
      && !empty($response->error)
      && !in_array(
        $response->code,
        ['200', '201', '202', '203', '204', '205', '206', '207', '208', '226']
      ));

    if ($not_success_code || $geotagging_debug_option == 2) {
      watchdog('geotagging',
        '@method: @code @status_message @protocol <br><b>Request</b>: <pre>@request</pre> <br><b>Body</b>:<br> @body <br><br><b>Headers</b>: <pre>@headers</pre>',
        [
          '@method' => $method,
          '@protocol' => isset($response->protocol) ? $response->protocol : '',
          '@code' => $response->code,
          '@status_message' => isset($response->status_message) ? $response->status_message : '',
          '@request' => isset($response->request) ? print_r($response->request, TRUE) : '',
          '@body' => isset($response->data) ? $response->data : '',
          '@headers' => isset($response->headers) ? print_r($response->headers, TRUE) : '',
          '@error' => isset($response->error) ? $response->error : '',
        ],
        WATCHDOG_DEBUG, $url);
    }

    return $response;
  }

}
