<?php

/**
 * @file
 * Menu callback.
 */

/**
 * Generates geotagging settings form.
 */
function geotagging_settings_form($form, &$form_state) {
  $form['server'] = [
    '#type' => 'fieldset',
    '#title' => t('Server options'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['server']['geotagging_endpoint_api_key'] = [
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#description' => t('This key will be used by endpoint client as http header X-API-KEY'),
    '#default_value' => variable_get(GEOTAGGING_X_API_KEY, GEOTAGGING_X_API_KEY_DEFAULT_VALUE),
  ];

  $form['server']['geotagging_endpoint_url'] = [
    '#title' => t('Geotagging endpoint url'),
    '#type' => 'textfield',
    '#description' => t('This key will be used by endpoint client as http header X-API-KEY'),
    '#default_value' => variable_get('geotagging_endpoint_url', ''),
  ];

  $form['server']['geotagging_basic_auth_enabled'] = [
    '#title' => t('Basic Auth required'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('geotagging_basic_auth_enabled', FALSE),
  ];

  $form['server']['geotagging_basic_auth_login'] = [
    '#title' => t('Basic Auth Login'),
    '#type' => 'textfield',
    '#default_value' => variable_get('geotagging_basic_auth_login', ''),
    '#states' => [
      'visible' => [
        'input[name="geotagging_basic_auth_enabled"]' => [
          'checked' => TRUE,
        ],
      ],
    ],
  ];

  $form['server']['geotagging_basic_auth_pass'] = [
    '#title' => t('Basic Auth Password'),
    '#type' => 'password',
    '#attributes' => ['value' => variable_get('geotagging_basic_auth_pass', '')],
    '#states' => [
      'visible' => [
        'input[name="geotagging_basic_auth_enabled"]' => [
          'checked' => TRUE,
        ],
      ],
    ],
  ];

  // Debug options.
  $form['debug'] = [
    '#type' => 'fieldset',
    '#title' => t('Logging options'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $debug_options = [
    0 => t('None'),
    1 => t('Failure'),
    2 => t('All'),
  ];

  $form['debug'][GEOTAGGING_DEBUG] = [
    '#type' => 'radios',
    '#title' => t('Choose the logging frequency of debugging messages for the chosen endpoint.'),
    '#options' => $debug_options,
    '#default_value' => variable_get(GEOTAGGING_DEBUG, 0),
  ];

  return system_settings_form($form);
}

