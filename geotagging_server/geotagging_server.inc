<?php

/**
 * @file
 * Menu callback.
 */

/**
 * Menu callback.
 */
function geotagging_server_access() {
  watchdog('geotagging_server', 'Access to geotagging endpoint', [], WATCHDOG_DEBUG);
  exit;
}

function geotagging_server_notify_clients($all) {
  $queue = DrupalQueue::get('geotagging_ping_client');
  foreach ($all as $row) {
    $queue->createItem($row);
  }
}

/**
 * Menu callback.
 */
function geotagging_server_operation($family_id = '', $hreflang = '') {
  if (in_array($_SERVER['REQUEST_METHOD'], ['GET', 'PUT', 'DELETE'])) {
    watchdog(
      'geotagging_server',
      '@method request to geotagging endpoint',
      ['@method' => $_SERVER['REQUEST_METHOD']],
      WATCHDOG_DEBUG
    );

    $missing_hreflang = empty($hreflang) && in_array($_SERVER['REQUEST_METHOD'], ['PUT', 'DELETE']);
    if (empty($family_id) || $missing_hreflang) {
      drupal_add_http_header('Status', '404 Not found');
      return;
    }

    /** @var GeotaggingServerEndpoint $endpoint */
    $endpoint = new GeotaggingServerEndpoint();

    $all = $endpoint->pull($family_id);

    switch ($_SERVER['REQUEST_METHOD']) {
      case 'GET':
        $result = [
          'id' => $family_id,
          'alternates' => [],
        ];
        if ($all) {
          $result['alternates'] = $all[$family_id];
        }
        if (empty($result['alternates'])) {
          drupal_add_http_header('Status', '404 Not found');
        }
        return $result;

      case 'PUT':
        $str = file_get_contents('php://input');
        if (!empty($str)) {
          $json = json_decode($str, TRUE);
          if (!is_null($json)) {
            if (empty($json['href'])) {
              drupal_add_http_header('Status', '400 Bad Request ');
              return ['msg' => 'Missing required field href'];
            }
            try {
              $result = $endpoint->put($family_id, $hreflang, $json['href']);
              drupal_add_http_header('Content-Type', 'application/json');
              switch ($result) {
                case GeotaggingServerEndpoint::PUT_OPERATION_RESULT_INSERTED:
                  drupal_add_http_header('Status', '201 Created');
                  break;

                case GeotaggingServerEndpoint::PUT_OPERATION_RESULT_UPDATED:
                  drupal_add_http_header('Status', '204 No Content');
                  break;
              }
              geotagging_server_notify_clients($all);
              $data = $endpoint->pull($family_id);
              return $data;
            }
            catch (GeotaggingServerEndpointUrlExistsException $e) {
              drupal_add_http_header('Status', '409 Conflict');
              return [
                'msg' => 'HRef already exists',
                'alternative' => [
                  'id' => $e->getContentFamilyId(),
                  'hreflang' => $e->getHreflang(),
                ],
              ];
            }
          }
        }
        drupal_add_http_header('Status', '400 Bad request');
        return ['msg' => 'JSON parse error'];

      case 'DELETE':
        if ($result = $endpoint->delete($family_id, $hreflang)) {
          geotagging_server_notify_clients($all);
          drupal_add_http_header('Status', '204 Partial Content');
        }
        else {
          drupal_add_http_header('Status', '404');
        }
        return;
    }
  }
  drupal_not_found();
  drupal_exit();
}