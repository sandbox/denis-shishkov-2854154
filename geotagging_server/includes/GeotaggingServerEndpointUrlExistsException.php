<?php

/**
 * @file
 * Contains GeotaggingServerEndpointUrlExistsException.
 */

/**
 * Class GeotaggingServerEndpointUrlExistsException.
 */
class GeotaggingServerEndpointUrlExistsException extends Exception {
  protected $hreflang;
  protected $contentFamilyId;

  /**
   * Get hreflang.
   *
   * @return string
   *   Hreflang like en_UK.
   */
  public function getHreflang() {
    return $this->hreflang;
  }

  /**
   * Set hreflang.
   *
   * @param string $hreflang
   *   Hreflang like en_UK.
   */
  public function setHreflang($hreflang) {
    $this->hreflang = $hreflang;
    return $this;
  }

  /**
   * Get Content family Id.
   *
   * @return string
   *   Content family Id.
   */
  public function getContentFamilyId() {
    return $this->contentFamilyId;
  }

  /**
   * Set Content family Id.
   *
   * @param string $contentFamilyId
   *   Content family Id.
   */
  public function setContentFamilyId($contentFamilyId) {
    $this->contentFamilyId = $contentFamilyId;
    return $this;
  }

}
