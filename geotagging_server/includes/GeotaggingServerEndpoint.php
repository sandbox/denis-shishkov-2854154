<?php

/**
 * @file
 * Class GeotaggingServerEndpoint.
 */

/**
 * Server endpoint implementation.
 */
class GeotaggingServerEndpoint {

  const PUT_OPERATION_RESULT_INSERTED = 1;
  const PUT_OPERATION_RESULT_UPDATED = 2;
  const PUT_OPERATION_RESULT_EXISTS = 3;

  protected $alternativeUrls = [];

  /**
   * Put operation.
   *
   * @param string $contentFamilyId
   *   Content family Id.
   * @param string $hreflang
   *   Hreflang like en_UK.
   * @param string $url
   *   Alternative URL.
   *
   * @return bool
   *   If operation was successfull.
   */
  public function put($contentFamilyId, $hreflang, $url) {
    $num = db_select('geotagging', 'jgm')
      ->fields('jgm', ['url'])
      ->condition('jgm.content_family_id', $contentFamilyId)
      ->condition('jgm.hreflang', $hreflang)
      ->countQuery()
      ->execute()
      ->fetchField();
    try {
      if ($num > 0) {
        db_update('geotagging')
          ->fields(['url' => $url])
          ->condition('content_family_id', $contentFamilyId)
          ->condition('hreflang', $hreflang)
          ->execute();
        return self::PUT_OPERATION_RESULT_UPDATED;
      }
      else {
        $result = db_insert('geotagging')
          ->fields([
            'content_family_id' => $contentFamilyId,
            'hreflang' => $hreflang,
            'url' => $url,
          ])
          ->execute();
        return self::PUT_OPERATION_RESULT_INSERTED;
      }
    }
    catch (Exception $e) {
      return self::PUT_OPERATION_RESULT_EXISTS;
    }
  }

  /**
   * Put operation.
   *
   * @param string $contentFamilyId
   *   Content family Id.
   *
   * @return string
   *   Urls with family id and hreflang.
   */
  public function pull($contentFamilyId) {
    $dbresult = db_select('geotagging', 'jgm')
      ->fields('jgm')
      ->condition('jgm.content_family_id', $contentFamilyId)
      ->execute()
      ->fetchAll();
    $result = [];
    foreach ($dbresult as $row) {
      if (!array_key_exists($row->content_family_id, $result)) {
        $result[$row->content_family_id] = [];
      }
      $result[$row->content_family_id][] = [
        'hreflang' => $row->hreflang,
        'href' => $row->url
      ];
    }
    return $result;
  }

  /**
   * Delete operation.
   *
   * @param string $contentFamilyId
   *   Content family Id.
   * @param string $hreflang
   *   Hreflang like en_UK.
   *
   * @return bool
   *   If operation was successfull.
   */
  public function delete($contentFamilyId, $hreflang) {
    return db_delete('geotagging')
      ->condition('hreflang', $hreflang)
      ->condition('content_family_id', $contentFamilyId)
      ->execute();
  }

  /**
   * Delete all records.
   *
   * @return bool
   *   If operation was successfull.
   */
  public function deleteAll() {
    return db_delete('geotagging')->execute();
  }

  /**
   * Returns array of all family ids with alternatice urls.
   *
   * @return array
   *   Alternative urls.
   */
  public function getAllEndpointData() {
    $dbresult = db_select('geotagging', 'jgm')
      ->fields('jgm')
      ->execute()
      ->fetchAll();
    $result = [];
    foreach ($dbresult as $row) {
      $result[$row->content_family_id][] = [
        $row->hreflang => $row->url,
      ];
    }
    return $result;
  }

}
